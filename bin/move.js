const path = require('path');
const fs = require('fs');
const {PROJECT_PATH, SRC_PATH, DIST_PATH, BIN_PATH, DESKTOP_PATH} = require('./config');

const quduo_dir_arr = ['web.public'];
const meigao_dir_arr = ['web.public.meigao'];
const dongfang_dir_arr = ['web.public.dongfang'];


module.exports = {
  moveDir,
  moveDist,
};

function moveDist(answer) {
  if (answer.platform === 'quduo') {
    moveDir(DIST_PATH, path.resolve(DESKTOP_PATH, `${quduo_dir_arr[0]}`, 'm'));
  }
  if (answer.platform === 'meigaomei') {
    moveDir(DIST_PATH, path.resolve(DESKTOP_PATH, `${meigao_dir_arr[0]}`, 'm'));
  }
  if (answer.platform === 'dongfanghui') {
    moveDir(DIST_PATH, path.resolve(DESKTOP_PATH, `${dongfang_dir_arr[0]}`, 'm'));
  }
}

function moveDir(src, dest) {
  let files = fs.readdirSync(src);

  files.forEach(async file => {
    let _src = path.join(src, `${file}`);
    let _dest = path.join(dest, `${file}`);

    let st = fs.statSync(_src);

    if (st.isFile()) {
      fs.copyFileSync(_src, _dest);
      console.log(`文件 ✿✿✿ ${file} ✿✿✿ 移动成功(☄⊙ω⊙)☄`);
    }

    if (st.isDirectory()) {
      exists(_src, _dest, moveDir);
    }
  })
}

function exists(src, dst, callback) {
  let exists = fs.existsSync(dst);
  if (exists) {
    callback(src, dst);
  } else {
    fs.mkdirSync(dst);////创建目录
    callback(src, dst)
  }
}

// 流的实现方式,但是是异步
// function read_write(src, dest, file) {
//   let readable;
//   let writeable;
//   return new Promise((resolve, reject) => {
//     readable = fs.createReadStream(src);
//     writeable = fs.createWriteStream(dest);
//     writeable.on('finish', (ex) => {
//       resolve(`文件 ✿✿✿ ${file} ✿✿✿ 移动成功(☄⊙ω⊙)☄`);
//       console.log(`文件 ✿✿✿ ${file} ✿✿✿ 移动成功(☄⊙ω⊙)☄`);
//     });
//     writeable.on('error', (ex) => {
//       reject(`文件 ✿✿✿ ${file} ✿✿✿ 移动失败(☄⊙ω⊙)☄`);
//       console.log(`文件 ✿✿✿ ${file} ✿✿✿ 移动失败(☄⊙ω⊙)☄`);
//     });
//     readable.pipe(writeable);
//   })
// }
