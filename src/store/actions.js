// 异步操作文件
import * as types from './mutation-types'
import {
  home_api, site_common_set, lotteryDetail_api,
  get_home_menu,
  get_home_forums
} from "@/common/api";
import store from '@/utils/sessionStorage';

export default {
  GET_HOME_MENU(context) {
    return get_home_menu().then(res => {
      context.commit({
        type: "SET_HOME_MENU",
        data: res
      });
    })
  },
  GET_HOME_FORUM(context) {
    return get_home_forums().then(res => {
      context.commit({
        type: "SET_HOME_FORUM",
        data: res
      });
    })
  },
  GET_HOME_INFO(context) {
    return Promise.all([
      site_common_set(),
      home_api()
    ]).then(([site, home]) => {
      if (site && site.code === 200) {
        context.commit({
          type: "SET_SITE_BASE_INFO",
          data: site.data
        });
      }

      if (home && home.code === 200) {
        let extra_data = {};

        extra_data.isShow = home.data.bankcard; // 是否绑定银行卡
        extra_data.isLogin = home.data.trier; // 登陆状态  yes为试玩账号  no 为正式用户
        extra_data.invi = home.data.invitation; // 登陆状态
        extra_data.pwd = home.data.password; // 是否设置登录密码
        extra_data.setPwd = home.data.fundpwd; // 是否设置资金密码
        extra_data.mask = home.data.mask; // 打码量
        home.data.mobile.length > 0
          ? (extra_data.mobile = home.data.mobile)
          : (extra_data.mobile = false); // 是否绑定手机号

        let assign_data = Object.assign({}, home.data, extra_data);

        context.commit({
          type: "INIT_DATA",
          data: assign_data
        });

        sessionStorage.setItem("msg", JSON.stringify(home.data));
        sessionStorage.setItem("isMobile", home.data.mobile); // 是否绑定手机号
        sessionStorage.setItem("invi", home.data.invitation); // 登陆状态
        sessionStorage.setItem("isLogin", home.data.trier); // 登陆状态
        sessionStorage.setItem("mask", home.data.mask); // 打码量
        sessionStorage.setItem("setPwd", home.data.fundpwd); // 是否设置资金密码
        sessionStorage.setItem("isShow", home.data.bankcard); // 是否绑定银行卡
        sessionStorage.setItem("realname", home.data.realname); // 是否有真实姓名

      } else {
        if (home.code === 400) {
          sessionStorage.setItem("isLogin", "false"); // 是否登录

          context.commit({
            type: "INIT_DATA",
            data: {
              isLogin: 'false',
            }
          });
        }
      }
      this.isLogin = sessionStorage.getItem("isLogin");
    })
  },
  amount({commit}, data) {
    commit(types.INIT_DATA, data)
  },
  GET_LOTTERY_DATA(context) {
    let lottery_id = store.get("lottery");

    return lotteryDetail_api({
      lottery_id
    }).then(res => {
        console.log(res);
        if (res.code === 200) {
          const {lottery_detail, lottery_gf_detail} = res.data;

          let first_gf = lottery_gf_detail[0];
          let first_xy = lottery_detail[0];

          context.commit({
            type: "SET_LOTTERY_DATA",
            data: res.data
          });

          context.commit({
            type: "SET_GF_LOTTERY_DATA",
            data: lottery_gf_detail
          });

          context.commit({
            type: "SET_XY_LOTTERY_DATA",
            data: lottery_detail
          });

          context.commit({
            type: "SET_CURRENT_GF_LOTTERY",
            data: first_gf
          });

          context.commit({
            type: "SET_CURRENT_XY_LOTTERY",
            data: first_xy
          });

          context.commit("ACTIVE_TYPE", {
            type: 'gf',
            name: [first_gf['name']]
          });

          context.commit("SET_CHECKED_ITEMS", []);

          context.commit("SET_LOTTERY_PATH", first_gf.lottery_path);

          let level = {level0: lottery_gf_detail};

          let index = 1;

          _loopType(first_gf);

          context.commit("LOTTERY_LEVEL", level);

          //loop函数，将层级取出来
          function _loopType(da) {

            if (da.children && da.children.length > 0) {
              level[`level${index}`] = da.children;
              index++;
              da.children.forEach(item => {
                _loopType(item);
              })
            }

            if (da.children && da.children.length === 0 && da.data) {
              return undefined;
            }

          }
        }
      }
    );
  }
}
