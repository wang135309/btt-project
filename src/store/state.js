// 状态文件
const state = {
  initData: {},
  userBasicInfo: {},
  siteBaseInfo: {
    download: "",
    invitation: "",
    register_enable: "",
    service_qq1: "",
    service_qq2: "",
    service_wechat: "",
  },
  lotteryData: {},
  lotteryGfData: [],
  lotteryXyData: {},
  currentGFLottery: {},
  currentXYLottery: {},
  activeType: {},
  lotteryLevel: {},
  lotteryPath: "",
  checkedItems: [], //每个页面选择的item总数
  lotteryTotal: {
    win: 0, //最多盈利
    total: [], //总注数
    money: 0, //总投注钱数
    single: 2,//单笔金额
    multiple: 1, //倍数
  },
  homeMenu: {
    activity_list: {},
    ads_above_list: {},
    auth_menu: [],
    slide_menu: [],
    slide_nav_menu: [],
  },
  homeForum: {
    todaypost: "0",
    posts: "0",
    threads: "0",
    forumlist: [],
  }
};

export default state
