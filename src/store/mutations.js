import * as types from "./mutation-types";

// 改变vuex中保存的状态值的文件
const mutations = {
  // 第一个数组为取出types中SET_SINGER作为方法名，第一个参数为当前状态，第二个参数提交给管理工具的值
  [types.INIT_DATA](state, payload) {
    state.initData = Object.assign({}, payload.data);
  },
  [types.UPDATE_DATA](state, updataData) {
    state.initData = Object.assign({}, state.initData, {
      ...updataData
    });
  },
  [types.SET_SITE_BASE_INFO](state, payload) {
    state.siteBaseInfo = Object.assign({}, state.siteBaseInfo, payload.data);
  },
  [types.SET_LOTTERY_DATA](state, payload) {
    state.siteBaseInfo = Object.assign({}, payload.data);
  },
  [types.SET_GF_LOTTERY_DATA](state, payload) {
    state.lotteryGfData = payload.data;
  },
  [types.SET_XY_LOTTERY_DATA](state, payload) {
    state.lotteryXyData = Object.assign({}, payload.data);
  },
  [types.SET_CURRENT_GF_LOTTERY](state, payload) {
    state.currentGFLottery = Object.assign({}, payload.data);
  },
  [types.SET_CURRENT_XY_LOTTERY](state, payload) {
    state.currentXYLottery = Object.assign({}, payload.data);
  },
  [types.ACTIVE_TYPE](state, payload) {
    state.activeType = Object.assign({}, state.activeType, payload);
  },
  [types.LOTTERY_LEVEL](state, payload) {
    state.lotteryLevel = Object.assign({}, payload);
  },
  [types.SET_LOTTERY_PATH](state, payload) {
    state.lotteryPath = payload;
  },
  [types.SET_CHECKED_ITEMS](state, payload) {
    state.checkedItems = payload;
  },
  [types.SET_LOTTERY_TOTAL](state, payload) {
    state.lotteryTotal = Object.assign({}, state.lotteryTotal, payload);
  },
  [types.SET_HOME_MENU](state, payload) {
    state.homeMenu = Object.assign({}, state.homeMenu, payload.data);
  },
  [types.SET_HOME_FORUM](state, payload) {
    state.homeForum = Object.assign({}, state.homeForum, payload.data);
  },
};

export default mutations;
