/**
 * Created by easterCat on 2019/4/19.
 */

//计算快三和值的总注数
export function totalCount(arr) {
  let total = [];
  arr.forEach(item => {
    let layer1, layer2, layer3;
    //和值的大小单双
    if (['大', '小', '单', '双'].includes(item)) {
      total.push([item, item, item]);
    } else if (item === '三同号通选') {
      total.push(['豹子', '豹子', '豹子']);
    } else if (['111', '222', '333', '444', '555', '666'].includes(`${item}`)) {
      total.push(item.split(''));
    } else if (item === '三连号') {
      total.push([1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6]);
    } else {
      for (let i = 1; i < item; i++) {
        layer1 = i;
        for (let j = 1; j < item; j++) {
          layer2 = j;
          for (let k = 1; k < item; k++) {
            layer3 = k;
            if ((layer1 + layer2 + layer3) === item) {
              total.push([layer1, layer2, layer3]);
            }
          }
        }
      }
    }
  });
  return total;
}


/**
 * 获得从m中取n的所有组合
 */
export function getFlagArrs(m, n) {
  if (!n || n < 1) {
    return [];
  }

  let resultArrs = [];
  let flagArr = [];
  let isEnd = false;
  let i, j, leftCnt;

  for (i = 0; i < m; i++) {
    flagArr[i] = i < n ? 1 : 0;
  }

  resultArrs.push(flagArr.concat());

  while (!isEnd) {
    leftCnt = 0;
    for (i = 0; i < m - 1; i++) {
      if (flagArr[i] == 1 && flagArr[i + 1] == 0) {
        for (j = 0; j < i; j++) {
          flagArr[j] = j < leftCnt ? 1 : 0;
        }
        flagArr[i] = 0;
        flagArr[i + 1] = 1;
        var aTmp = flagArr.concat();
        resultArrs.push(aTmp);
        if (aTmp.slice(-n).join("").indexOf('0') == -1) {
          isEnd = true;
        }
        break;
      }
      flagArr[i] == 1 && leftCnt++;
    }
  }
  return resultArrs;
}

