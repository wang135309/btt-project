import axios from 'axios';
import qs from 'qs';
import {
  Toast,
  Indicator,
  MessageBox
} from 'mint-ui';
import {
  debug
} from 'util';
import router from '../router/index';

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
// axios.defaults.baseURL = 'http://api.front.com';
axios.defaults.timeout = 50000;

/*
 *  若需拦截器可在此封装
 */
axios.interceptors.response.use(function (res) {
  // Do something with response data
  if (res.data.code === 503) {
    return new Promise((resolve, reject) => {
      MessageBox({
        title: '提示',
        message: res.data.msg,
        showCancelButton: false,
        showConfirmButton: false,
      });
      resolve('成功');
    }).then((value) => {
      router.push(`/404?msg=${res.data.msg}`);
      // window.location = 'http://127.0.0.1:8080/#/login';
    });
  }

  return res;
}, function (error) {
  // Do something with response error
  return Promise.reject(error);
});

//返回一个Promise
export function fetch(url, params, type) {
  // url = url + "?XDEBUG_SESSION_START=11044";
  params = Object.assign({}, params,)
  return new Promise((resolve, reject) => {
    if (type == 'post') {
      //因后台post请求需form-data传参，所以使用qs.stringify
      axios.post(url, qs.stringify(params))
        .then(response => {
          if (response.data.code == 401) {

            //在登录页就不要烦人的弹了
            if (!/login/.test(window.location.href)) {
              MessageBox('提示', '您的账号在其他设备登录，如非本人操作，请联系客服处理');
            }
            router.push('login')
          }
          resolve(response.data);
        }, err => {
          Indicator.close();
          // Toast('网络故障，请稍后再试')
          reject(err);
          localStorage.setItem({
            'err': err
          })
        })
        .catch((error) => {
          reject(error)
        })
    } else {
      axios.get(url, {
        params
      })
        .then(response => {
          resolve(response.data);
        }, err => {
          Indicator.close();
          // Toast('网络故障，请稍后再试')
          reject(err);
        })
        .catch((error) => {
          reject(error)
        })
    }
  })
}

export function fmoney(s, n) {
  /*
   * 参数说明：
   * s：要格式化的数字
   * n：保留几位小数
   * */
  n = n > 0 && n <= 20 ? n : 2;
  s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
  var l = s.split(".")[0].split("").reverse(),
    r = s.split(".")[1];
  var t = "";

  for (var i = 0; i < l.length; i++) {
    t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
    // t=t.split("").reverse()

  }
  t = t.split("").reverse().join("") + "." + r;
  if (t[0] == ',') {
    t = t.replace(',', '')
  }
  return t
}

//获取emoji头像(本地json只能放在static文件夹里)
export function getEmojiData() {
  return axios({
    method: 'get',
    url: '/static/emoji.json',
  })
    .then(function (res) {
      return Promise.resolve(res.data);
    });
}

export const t = {
  start: new Date() / 1000,
  end: new Date() / 1000 - 604800
};

export function colors(data) {
  if ((typeof data == 'object') && data.constructor == Array) {
    data.forEach(element => {
      if ((typeof element.award_number == 'string') && element.award_number.constructor == String) {
        element.award_number = element.award_number.split(",");
      }
      element.colors = [];
      for (let i = 0; i < element.award_number.length; i++) {
        switch (element.award_number[i]) {
          case "大":
            element.colors.push({
              num: "大",
              color: "#fe3959"
            });
            break;
          case "小":
            element.colors.push({
              num: "小",
              color: "#3D71FF"
            });
            break;
          case "单":
            element.colors.push({
              num: "单",
              color: "#fe3959"
            });
            break;
          case "双":
            element.colors.push({
              num: "双",
              color: "#3D71FF"
            });
            break;
          case "龙":
            element.colors.push({
              num: "龙",
              color: "#fe3959"
            });
            break;
          case "虎":
            element.colors.push({
              num: "虎",
              color: "#3D71FF"
            });
            break;
          default:
            element.colors = null;
            break;
        }
      }
    });
  }
}

export function stringColors(data) {
  if ((typeof data == 'object') && data.constructor == Array) {
    data.forEach(element => {
      if ((typeof element.award_number == 'string') && element.award_number.constructor == String) {
        element.award_number = element.award_number.split(",");
      }


      element.colors = [];

      if (element.hasOwnProperty('award_number') && element.award_number) {

        for (let i = 0; i < element.award_number.length; i++) {

          switch (element.award_number[i]) {
            case "1":
              element.colors.push({
                num: '1',
                color: "#ECCA00"
              });
              break;
            case "2":
              element.colors.push({
                num: '2',
                color: "#3CA2ED"
              });
              break;
            case "3":
              element.colors.push({
                num: '3',
                color: "#606060"
              });
              break;
            case "4":
              element.colors.push({
                num: '4',
                color: "#E8803F"
              });
              break;
            case "5":
              element.colors.push({
                num: '5',
                color: "#20C5D3"
              });
              break;
            case "6":
              element.colors.push({
                num: '6',
                color: "#5C57CC"
              });
              break;
            case "7":
              element.colors.push({
                num: '7',
                color: "#999999"
              });
              break;
            case "8":
              element.colors.push({
                num: '8',
                color: "#FE3959"
              });
              break;
            case "9":
              element.colors.push({
                num: '9',
                color: "#A23F4B"
              });
              break;
            case "10":
              element.colors.push({
                num: '10',
                color: "#5FCC49"
              });
              break;
            case "大":
              element.colors.push({
                num: "大",
                color: "#fe3959"
              });
              break;
            case "小":
              element.colors.push({
                num: "小",
                color: "#3D71FF"
              });
              break;
            case "单":
              element.colors.push({
                num: "单",
                color: "#fe3959"
              });
              break;
            case "双":
              element.colors.push({
                num: "双",
                color: "#3D71FF"
              });
              break;
            case "龙":
              element.colors.push({
                num: "龙",
                color: "#fe3959"
              });
              break;
            case "虎":
              element.colors.push({
                num: "虎",
                color: "#3D71FF"
              });
              break;
            default:
              element.colors = null;
              break;
          }
        }
      }

      if (element.hasOwnProperty('longhu')) {
        for (let i = 0; i < element.longhu && element.longhu.length; i++) {
          switch (element.longhu[i]) {
            case "1":
              element.colors.push({
                num: '1',
                color: "#ECCA00"
              });
              break;
            case "2":
              element.colors.push({
                num: '2',
                color: "#3CA2ED"
              });
              break;
            case "3":
              element.colors.push({
                num: '3',
                color: "#606060"
              });
              break;
            case "4":
              element.colors.push({
                num: '4',
                color: "#E8803F"
              });
              break;
            case "5":
              element.colors.push({
                num: '5',
                color: "#20C5D3"
              });
              break;
            case "6":
              element.colors.push({
                num: '6',
                color: "#5C57CC"
              });
              break;
            case "7":
              element.colors.push({
                num: '7',
                color: "#999999"
              });
              break;
            case "8":
              element.colors.push({
                num: '8',
                color: "#FE3959"
              });
              break;
            case "9":
              element.colors.push({
                num: '9',
                color: "#A23F4B"
              });
              break;
            case "10":
              element.colors.push({
                num: '10',
                color: "#5FCC49"
              });
              break;
            case "大":
              element.colors.push({
                num: "大",
                color: "#fe3959"
              });
              break;
            case "小":
              element.colors.push({
                num: "小",
                color: "#3D71FF"
              });
              break;
            case "单":
              element.colors.push({
                num: "单",
                color: "#fe3959"
              });
              break;
            case "双":
              element.colors.push({
                num: "双",
                color: "#3D71FF"
              });
              break;
            case "龙":
              element.colors.push({
                num: "龙",
                color: "#fe3959"
              });
              break;
            case "虎":
              element.colors.push({
                num: "虎",
                color: "#3D71FF"
              });
              break;
            default:
              element.colors = null;
              break;
          }
        }
      }
    });
  }

}

export function luhnCheckBankCard(cardNo) {
  if (!cardNo) return false;
  if (cardNo.length < 15) return false;
  if (cardNo.length > 19) return false;

  let num_arr = cardNo.split('').map(num => Number(num));

  let oddNum = num_arr.filter((num, index) => (index + 1) % 2 === 1)
    .reduce((total, value) => {
      total = total + value;
      return total;
    });

  let evenNum = num_arr.filter((num, index) => (index + 1) % 2 === 0)
    .map(num => {
      let new_num = num * 2;
      if (new_num >= 10) return new_num - 9;
      return new_num;
    })
    .reduce((total, value) => {
      total = total + value;
      return total;
    });
  console.log(evenNum + oddNum)
  return (evenNum + oddNum) % 10 === 0;
}

// 试玩登录
export function shiwan(fun, path, msg) {
  let isLogin = sessionStorage.getItem("isLogin");
  if (isLogin == "false") {
    MessageBox.confirm("您还未登录", "提示", {
      closeOnClickModal: false
    })
      .then(action => {
        fun.push("login");
      })
      .catch(res => {
        fun.push("center");
      });
  } else if (isLogin == "yes") {
    MessageBox.confirm("正式会员才可以使用该功能", "提示", {
      confirmButtonText: "成为会员"
    })
      .then(action => {
        fun.push("reg");
      })
    // .catch(res => {
    //   fun.go(-1);
    // });
  } else {
    if (path == "unBound") {
      this.mask = sessionStorage.getItem("mask"); //打码量
      let setPwd = sessionStorage.getItem("setPwd");
      if (setPwd == "false") {
        MessageBox.confirm("您还未设置资金密码哟！", "提示", {
          closeOnClickModal: false
        })
          .then(action => {
            fun.push("setPwd");
          })
          .catch(res => {
            // window.history.back();
          });
      } else if (this.mask != 0) {
        MessageBox.confirm(
          "您还有" +
          this.mask +
          "打码量未完成,请完成后再进行提现</br><small style='color:red'>(注: 视讯及电子游戏打码量延时10分钟结算)</small>",
          "提示", {
            closeOnClickModal: false,
            showCancelButton: true,
            cancelButtonText: '打码量详情'
          }
        )
          .then(action => {

          })
          .catch(res => {
            fun.push('maskDetail');
          });
      } else {
        fun.push(path);
      }
    } else {
      fun.push({
        name: path,
        query: {
          id: msg
        }
      });
    }
  }
}
