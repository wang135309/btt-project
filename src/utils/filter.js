import moment from "moment";

export function filter_pay_type(text) {
  return text && text === 'alipay' ?
    '支付宝' :
    text === 'wxpay' ?
      '微信支付' :
      text === 'qq' ?
        'QQ钱包' :
        text === 'bank' ?
          '银行' :
          '';
}

export function filter_game(text) {
  return text && text === 'lottery' ?
    '彩票' :
    text === 'ag' ?
      'AG视讯' :
      text === 'sb' ?
        '申博视讯' :
        text === 'rt' ?
          '电子游艺' :
          '';
}

export function filter_amount(text) {
  if (text) {
    if (typeof text === 'string') {
      if (text === '0') {
        return '0.00';
      } else if (text === '0.00') {
        return '0.00';
      } else if (text.indexOf('.') >= 0) {
        let arr = text.split('.');
        let after = arr[1].length === 0 ?
          '00' :
          arr[1].length === 1 ?
            `${arr[1]}0` :
            arr[1].length === 2 ?
              `${arr[1]}` :
              arr[1].length > 2 ?
                `${arr[1].slice(0, 2)}` :
                '';
        return `${arr[0]}.${after}`
      } else {
        return text;
      }
    } else {
      let a = String(text).split('.')[0];
      let b = String(text).split('.')[1] ? String(text).split('.')[1] : '00';
      if (b.length > 0) {
        b = b.slice(0, 2);
        return `${a}.${b}`;
      } else {
        return a;
      }
    }
  } else {
    return '';
  }
}

export function filter_amount_add_zero(text) {
  text = `${text}`;
  if (text) {
    let arr = text.split('.');
    if (arr[1]) {
      return arr[1].length === 1 ? `${text}0` : text;
    } else {
      return `${text}.00`;
    }
  }
}

export function filter_status(text) {
  return text && text === 'pending' ? '正在充值' : text === 'success' ? '充值成功' : text === 'failure' ? '充值失败' : ''
}

export function filter_status11(text) {
  return text && text === 'pending' ? '等待' : text === 'success' ? '成功' : text === 'failure' ? '失败' : ''
}

export function filter_status6(text) {
  return text && text === 'pending' ? '待审核' : text === 'success' ? '提现成功' : text === 'failure' ? '提现失败' : ''
}

export function filter_status10(text) {
  return text && text === 'pending' ? '待审核' : text === 'success' ? '转账成功' : text === 'failure' ? '转账失败' : ''
}

//活动记录的状态
export function filter_status7(text) {
  return text && text === 'pending' ? '未发放' : text === 'success' ? '已发放' : text === 'reject' ? '已拒绝' : ''
}

//日工资的状态
export function filter_status14(text) {
  return text && text === 'pending' ? '未发放' : text === 'success' ? '已发放' : text === 'void' ? '未达标' : ''
}

export function filter_status16(text) {
  return text && text === 'pending' ? '未发放' : text === 'sent' ? '已发放' : text === 'canceled' ? '已取消' : ''
}

export function filter_status17(text) {
  return text && text === '0' ?
    '未开奖' : text === '1' ?
      '已中奖' : text === '2' ?
        '未中奖' : text === '3' ?
          '撤单' : text === '4' ?
            '系统撤单' : ''
}

//签约分红的状态
export function filter_status15(text) {
  return text && text === 'pending' ? '未签约' : text === 'success' ? '已签约' : text === 'void' ? '待确认' : ''
}

//活动记录的状态
export function filter_status8(text) {
  let arr = [{text: '人工发放', value: '99'},
    {text: '立即发放', value: '0'},
    {text: '1日后发放', value: '1'},
    {text: '2日后发放', value: '2'},
    {text: '3日后发放', value: '3'},
    {text: '4日后发放', value: '4'},
    {text: '5日后发放', value: '5'},
    {text: '6日后发放', value: '6'},
    {text: '7日后发放', value: '7'},
    {text: '8日后发放', value: '8'},
    {text: '9日后发放', value: '9'},
    {text: '10日后发放', value: '10'}];

  let new_arr = arr.filter(item => {
    return item.value === `${text}`;
  });

  return new_arr[0] && new_arr[0]['text'];
}

export function filter_status2(text) {
  return text && text === 'open' ? '启用' : text === 'close' ? '停用' : '';
}

export function filter_layer(text) {
  return text && text === '1' ? '下一级' : '下二级';
}

export function filter_status12(text) {
  return text && text === 'user' ? '会员' : text === 'agent' ? '代理' : '';
}

export function filter_status13(text) {
  return text && text === 'success' ? '正常' : text === 'failure' ? '冻结' : '';
}

//银行卡列表状态
export function filter_status3(text) {
  return text && text === 'open' ? '正常' : text === 'close' ? '冻结' : text === 'delete' ? '已解绑' : '';
}

//用户管理的状态
export function filter_status4(text) {
  return text && text === 'open' ? '正常' : text === 'close' ? '封号' : '';
}

//用户管理的在线状态
export function filter_status9(text) {
  if (text === 0) {
    return '离线'
  } else if (text === 1) {
    return '在线';
  }
  return '';
}

export function filter_status5(text) {
  return text && text === 'open' ? '正常' : text === 'close' ? '冻结' : text === 'delete' ? '已解绑' : '';
}

export function filter_time(text) {
  return text && text !== '0' ? `${moment.unix(Number(text)).format("YYYY-MM-DD HH:mm:ss")}` : null;
}

export function filter_time2(text) {
  return text && text !== '0' ? `${moment.unix(Number(text)).format("YYYY-MM-DD")}` : null;
}

//获取分红的期数
export function filter_day(text) {
  if (text && text !== '0' && text !== 0) {
    let date = new Date(text);
    let y = date.getFullYear();
    let m = date.getMonth() + 1;
    let d = date.getDate();
    if (d <= 15) {
      return `${y}-${m}-1 至 ${y}-${m}-15`
    } else if (d > 15) {
      return `${y}-${m}-16 至 ${y}-${m}-30`
    } else {
      return '';
    }
  }
}

export function filter_scope(text) {
  return text && text === 'all' ? '全部用户' : text === 'point' ? '指定用户' : text === 'group' ? '用户等级' : '';
}
