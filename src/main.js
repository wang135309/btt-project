// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import VueAwesomeSwiper from 'vue-awesome-swiper';
import VueQuillEditor from 'vue-quill-editor'
import App from "./App";
import router from "./router";
import Mint from "mint-ui";
import _ from "lodash";
import "mint-ui/lib/style.css";
import "../static/mycss.css";
import "../src/vlayui/fontello-1de7de7c/css/fontello.css";
import 'swiper/dist/css/swiper.css'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
import VueClipboard from "vue-clipboard2";
import VueGoodTablePlugin from "vue-good-table";
import {InfiniteScroll, DatetimePicker} from "mint-ui";
import moment from "moment/moment";
import {fmoney} from "@/common/config";
import store from "./store";

import Mui from "vue-awesome-mui";

Vue.use(Mui);
Vue.use(VueGoodTablePlugin);
Vue.use(InfiniteScroll, DatetimePicker);
VueClipboard.config.autoSetContainer = true;
Vue.use(VueAwesomeSwiper);
Vue.use(VueQuillEditor)

Vue.use(VueClipboard);
Vue.use(Mint);
Vue.use(_);
Vue.config.productionTip = false;
Vue.filter("moment", function (value, formatString) {
  formatString = formatString || "YYYY-MM-DD HH:mm:ss";
  // return moment(value).format(formatString); // value可以是普通日期 20170723
  return moment.unix(value).format(formatString); // 这是时间戳转时间
});

Vue.filter("fmoney", function (params) {
  return fmoney(params);
});

// router.beforeEach((to, from, next) => {
//   if (
//     to.path == '/clause' ||
//     to.path == "/center" ||
//     to.path == "/login" ||
//     to.path == "/reg" ||
//     to.path == "/signIn" ||
//     to.path == "/personal" ||
//     to.path == "/kefu" ||
//     to.path == "/transfer" ||
//     to.path == "/download" ||
//     to.path == "/share" ||
//     to.path == "/hall" ||
//     to.path == "/promotion" ||
//     to.path == "/systemMsgCenter" ||
//     to.path == "/promotionDetail" ||
//     to.path == "/forgotPwd" ||
//     to.path == "/announcementDetail"
//   ) {
//     next();
//   } else {
//     var isLogin = sessionStorage.getItem("isLogin");
//     if (!isLogin || isLogin == "false") {
//       next({
//         path: "/login"
//       });
//     } else {
//       next();
//     }
//   }
// });

Array.prototype.remove = function (val) {
  var index = this.indexOf(val);
  if (index > -1) {
    this.splice(index, 1);
  }
};

/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  store,
  components: {
    App
  },
  template: "<App/>"
});
