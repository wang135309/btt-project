## header

#### prop

| prop       |     作用      |  类型   |  default |
| :--------- | :-----------: | :-----: | -------: |
| border     | 是否有 border | Boolean |    false |
| title      |     标题      | string  |     none |
| titleAlign |   标题位置    | string  |   center |
| theme      |     主题      | string  |    light |
| position   |     定位      | string  | absolute |
| shadow     |     阴影      | Boolean |    false |

> theme => 'light','stable','positive','calm','balanced','energized','assertive','dark'
