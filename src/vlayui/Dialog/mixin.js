import LayBackdrop from "../Backdrop";

const popup_enter_duration = 200;
const popup_leave_duration = 200;

const extend = (target, source) => {
  for (let key in source) {
    target[key] = source[key]
  }

  return target
};

export default {
  destroyed() {
    let parent = this.$el.parentNode;
    parent.removeChild(this.$el);
  },
  methods: {
    show(options) {
      extend(this, options);

      if (LayBackdrop.getState() === 0) LayBackdrop.show();

      this.state = 1;

      this.promise = new Promise((resolve, reject) => {
        this.$on('AlertOkEvent', () => {
          this.hide(resolve)
        })
      });

      document.body.classList.add('popup-open');
      return this.promise
    },
    hide(callback) {
      let length = document.querySelectorAll('[lay-dialog]').length;
      if (length === 1) { // 只剩最后一个dialog实例的时候，backdrop才隐藏
        LayBackdrop.hide();
        document.body.classList.remove('popup-open')
      }

      this.state = 2;
      setTimeout(() => {
        this.state = 0;
        this.$destroy();
        callback && callback();
      }, popup_leave_duration)
    },

    onOk() {
      this.$emit('AlertOkEvent')
    }
  }
}
